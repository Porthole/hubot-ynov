const code = require("./bro-code.json");

module.exports = (robot) => {
  robot.hear(/bro\s?([0-9]*)?/i, (res) => {
      let index = res.match[1];
      if (index) {
        index = Number.parseInt(index);
        if (index <= code.length) {
          res.send(code[index - 1]);
        } else {
          res.send("Ta cru c'était la bible frère ?");
        }
      } else {
        res.send(res.random(code));
      }
    }
  );
};
