FROM node:latest
RUN mkdir -p /opt/hubot
WORKDIR /opt/hubot
COPY . .
RUN npm install
CMD ./hubot-slack
